# Description

The [Docker](http://docker.com)  image contains a LEMP package with pre-installed `nginx (1.11.1) + php-fpm (7.0.8) + mysql (5.7.13)`. It can be used as a php development server.

## Features

1. Nginx is configured to automatically resolve all domains ending with `.dev`. *It is very convenient:* you can simply create the directory `app` (domain name **without** `.dev`) in the web document root and it will be available by the link `http://app.dev/` (NOTICE: it requires some preliminary 
configuration, see [Additional information](#markdown-header-dnsmasq) for detailed information). 

2. Pre-installed `composer` (with assets plugin), `git`, `phpmyadmin` (by default, automatic login as `root` without password), etc.

3. It stores source codes and mysql data on the host machine, therefore you don't need to deploy your app each time you start container. 

4. Php process run by user with UID 1000 (by default, the same UID as your host machine user), thus it can access all files. At the same time, you have all permissions for generated files.

5. Default password for root user - `root`.

6. SSH service starts automatically on container startup.
Defualt auth credentials for ssh:
    - host: `172.17.0.2`
    - port: `2202`
    - login: `root`
    - pass: `root`
7. To access container via ssh use `root` user or add your public key with command `ssh-copy-id -p 2202 root@172.17.0.2`.

8. PHP [MessDetector](https://phpmd.org/) and [CodeSniffer](http://pear.php.net/package/PHP_CodeSniffer) with [Yii2 Coding Standards](https://github.com/yiisoft/yii2-coding-standards) already pre-installed. 

# Instruction

## How to run container

0. First of all you need install [Docker](https://docs.docker.com/linux/) on your machine. For convinience, I recommend you to add your user to *docker* group, otherwise, all command need be run as root.

1. Run container
`docker run --name web-server -p 127.0.0.1:80:80 -v /opt/mysql:/var/lib/mysql -v /var/www:/web -d romangonchar/php-web-server`

##### Commands

`--name web-server` set container name
For simple access to it in the future: you can *start*, *restart* and *stop* the created container using its name: `docker start web-server`

`-p 127.0.0.1:80:80` bind the port `80` of the container to the port `80` on the host machine (on 127.0.0.1 localhost)

`-v /opt/mysql:/var/lib/mysql` bind mount a volume of mysql data directory from host machine (`/opt/mysql`) to
container (`/var/lib/mysql`)

`-v /var/www:/web` bind mount a volume of source codes from host machine (`/var/www`) to container (`/web`)

`-d` detached mode: run the container in the background and print the new container ID

##### Optional:

`-m 512M` memory limit

`--memory-swap 1G` limit SWAP

*Important!* Virtual SWAP limitations are supported by your kernel. 
Commands like `top` or `free` will not show you the limit number due to docker specifications. To see the real limited RAM use command 
`cat /sys/fs/cgroup//memory/memory.limit_in_bytes`

`--ip 172.30.100.104` container IP address

`--help` show all available commands for `run` command

## Terminal access to container

You can access container terminal like this:

    docker exec -ti dev-server su docker
    
It logs you in the container as special user `docker`, witch can work with source code and console commands.

But if you need `root` privileges, run:

    docker exec -ti dev-server bash
    
It logs you in as `root`

# Additional information

## DNSMasq

For comfortable work, I suggest you to use [dnsmasq](https://en.wikipedia.org/wiki/Dnsmasq) for automatic resolving of such domains as `http://*.dev/`. You can install it in Ubuntu this way (run as `root`):

    apt-get install dnsmasq
    echo "address=/.dev/127.0.0.1" | sudo tee /etc/dnsmasq.conf
    
By default container binds `80` port on local `127.0.0.1:80`. If you are using another binding, change the 
ip address in the previous code to the one you use.

Other variant is to update `/etc/hosts` manually, each time adding something like this:

    127.0.0.1   app.dev

## Install new software

Before you can install some new packages, you need run (as `root`):

    apt-get update

Because all packages cache was cleaned while image was building

## Custom scripts

If you have some additional scripts that should be executed with container, just put them in `extenstions` folder and add ti `Dockerfile`

# Contribution and feedback

## Supported Docker versions

This image is officially supported on Docker version 1.10.3.
Support for older versions (down to 1.0) is provided on a best-effort basis.

## Author

**Roman Gonchar** - <roman.gonchar92@gmail.com>

*Special thanks to [MetalGuardian](https://github.com/metalguardian/)*